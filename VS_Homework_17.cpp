﻿// VS_Homework_17.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <cmath>

class Character
{
public:
    Character(std::string _CharacterName, std::string _BookTitle, std::string _BookAuthor, int _CreationYear, bool _FilmAdaptation) :
        CharacterName(_CharacterName), BookTitle(_BookTitle), BookAuthor(_BookAuthor), CreationYear(_CreationYear), FilmAdaptation(_FilmAdaptation)
    {}
    void GetCharInf()
    {
        std::cout << "Character's name: "  << CharacterName << std::endl
            << "Title of the book: " << BookTitle << std::endl
            << "Book author: " << BookAuthor << std::endl
            << "Year of character creation: " << CreationYear << std::endl;
        if (FilmAdaptation)
        {
            std::cout << "The book was adapted for the screen" << std::endl;
        }
        else
        {
            std::cout << "The book wasn't adapted for the screen" << std::endl;
        }
    }
private:
    std::string CharacterName;
    std::string BookTitle;
    std::string BookAuthor;
    int CreationYear;
    bool FilmAdaptation;
};

class Vector
{
public:
    Vector(): x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        std::cout << x << ' ' << y << ' ' << z << '\n';
    }
    double VectorAbsVal()
    {
        return sqrt((pow(x, 2) + pow(y, 2) + pow(z, 2)));
    }

private:
    double x;
    double y;
    double z;
};


int main()
{
    Character first("Sherlock Holmes", "A Study in Scarlet", "Sir Arthur Conan Doyle", 1887, true);
    Character second("Alice", " Alice's Adventures in Wonderland", "Lewis Carroll", 1865, true);
    Character third("Holden Caulfield", "The Catcher in the Rye", "Jerome David Salinger", 1951, false);

    std::cout << std::endl;
    first.GetCharInf();
    std::cout << std::endl;
    second.GetCharInf();
    std::cout << std::endl;
    third.GetCharInf();
    std::cout << std::endl;
        
    Vector v( 5, 7, 3);
    std::cout << "Vector coordinates are ";
    v.Show();
    std::cout << "Absolute value of a vector is " << v.VectorAbsVal();
    std::cout << std::endl;

    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
